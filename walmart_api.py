from collections import defaultdict
from flask import Flask
from flask.ext.restful import reqparse, abort, Api, Resource
import os
import json

settings = {}
maps = {}

class Graph(object):
    """Graph class containing all node locations and routes of a map."""
    def __init__(self):
        self.edges = defaultdict(list)

    def add_edge(self, from_node, to_node, distance):
        """Adds a new route from an origin to a destination, expliciting its distance (weight)."""
        self.edges[from_node].append((to_node, distance))
        # By default, it's assumed the distance of A->B is the same of B->A. However, this is not always
        # the case. Therefore, this behavior can be changed by setting to false the setting below.
        if settings['sameReturnDistance']:
            self.edges[to_node].append((from_node, distance))

    def node_in_route(self, node, route):
        """Verifies if a node is already in a route, to avoid cyclic redundancy."""
        for visited in route:
            if node == visited[0]:
                return True
        return False

    def find_all_routes(self, start, end, route=[]):
        """Calculates all possible routes from a starting node, and it's distances."""
        route = route + ([(start, 0)] if type(start) is str else [(start[0],start[1])])
        if start[0] == end:
            return [route]
        if not self.edges.has_key(start[0]):
            return []
        routes = []
        for node in self.edges[start[0]]:
            if not self.node_in_route(node[0], route):
                newroutes = self.find_all_routes(node, end, route)
                for newroute in newroutes:
                    routes.append(newroute)
        return routes

    def find_shortest_route(self, start=None, end=None):
        """Finds the shortest route from the list of routes provided by find_all_routes method."""
        if start and end:
            routes = self.find_all_routes(start, end)

        short_route = (None, 0)

        for route in routes:
            new_route = ""
            distance = 0
            for node in route:
                distance += node[1]
                new_route += node[0] + " "
            if not short_route[0] or distance < short_route[1]:
                short_route = (new_route.strip(), distance)

        return short_route

class ShortestRoute(Resource):
    """Flask-restful resource attached to the endpoint shortest, for finding the shortest route"""
    global settings
    global maps
    parser = reqparse.RequestParser()
    parser.add_argument('mapName', type=str, required=True)
    parser.add_argument('origin', type=str, required=True)
    parser.add_argument('destiny', type=str, required=True)
    parser.add_argument('economy', type=float, required=True)
    parser.add_argument('fuelPrice', type=float, required=True)

    def get(self):
        args = self.parser.parse_args()
        try:
            current_map = maps[args['mapName']]
        except KeyError:
            file_path = os.path.join(settings['mapDir'], args['mapName'] + settings['mapExtension'])
            if os.path.isfile(file_path):
                f = open(file_path, 'r')
                current_map = f.read()
                f.close()
            else:
                return {'error': 'File not found', 'message': 'Map ' + args['mapName'] + ' was not found.'}, 404

        g = Graph()
        for edge in current_map.split('\n'):
            orig, dest, dist = edge.split(' ')
            g.add_edge(orig, dest, int(dist))

        result = g.find_shortest_route(args['origin'], args['destiny'])
        if result[0]:
            cost = result[1]/args['economy']*args['fuelPrice']
            cost = "{0:.2f}".format(round(cost,2))

            return {"route":result[0], "cost":cost}
        else:
            return {'error': 'No route found', 'message': 'No route was found for ' + args['origin'] +
            '->' + args['destiny']}, 404

class AddMap(Resource):
    """Flask-restful resource attached to the endpoint addmap, for adding a new map file"""
    global settings
    parser = reqparse.RequestParser()
    parser.add_argument('map', type=str, required=True)
    parser.add_argument('name', type=str, required=True)

    def post(self):
        args = self.parser.parse_args()
        file_path = os.path.join(settings['mapDir'], args['name'] + settings['mapExtension'])
        if os.path.isfile(file_path) and not settings['allowOverwrite']:
            return {'error': 'File already exists.', 'message': 'Map file already exists. If you wish'
            ' to update or replace it, please refer to the Docs and change the settings property'
            ' \'allowOverwrite\' to True.'}, 409
        else:
            if not os.path.isdir(settings['mapDir']):
                os.makedirs(settings['mapDir'])
            f = open(file_path, 'w')
            f.write(args['map'])
            f.close()
        return {'success': 'Map ' + args['name'] + ' added.'}

class GetAllMaps(Resource):
    """Lists all available maps"""
    def get(self):
        maps = []
        for fname in os.listdir(settings['mapDir']):
            if fname.endswith(settings['mapExtension']):
                maps.append(fname[:-len(settings['mapExtension'])])
        return maps

class GetMap(Resource):
    """Show an specific map, with all nodes and distances"""
    def get(self, map_name):
        try:
            current_map = maps[map_name]
        except KeyError:
            file_path = os.path.join(settings['mapDir'], map_name + settings['mapExtension'])
            if os.path.isfile(file_path):
                f = open(file_path, 'r')
                current_map = f.read()
                f.close()
            else:
                return {'error': 'File not found', 'message': 'Map ' + map_name + ' was not found.'}, 404

        return {map_name: current_map}

class Settings(Resource):
    """Flask-restful resource attached to the endpoint settings, for checking and updating settings file."""
    global settings
    parser = reqparse.RequestParser()
    parser.add_argument('allowOverwrite', type=bool)
    parser.add_argument('mapExtension', type=str)
    parser.add_argument('sameReturnDistance', type=bool)
    parser.add_argument('preLoadMaps', type=bool)
    parser.add_argument('mapDir', type=str)

    def get(self):
        return settings

    def post(self):
        args = self.parser.parse_args()
        for key in args.keys():
            if args[key] is not None:
                settings[key] = args[key]
        f = open('settings', 'w')
        json.dump(settings, f)
        f.close()
        return settings

def load_maps():
    """Loads all map files in the configured maps directory if the parameter preLoadMaps is True."""
    for fname in os.listdir(settings['mapDir']):
        if fname.endswith(settings['mapExtension']):
            file_path = os.path.join(settings['mapDir'], fname)
            f = open(file_path, 'r')
            maps[fname[:-len(settings['mapExtension'])]] = f.read()
            f.close()


#If the settings file is not present, create a new one with the default values
if not os.path.isfile('settings'):
    f = open('settings', 'w')
    settings['allowOverwrite'] = False
    settings['mapExtension'] = '.map'
    settings['sameReturnDistance'] = True
    settings['preLoadMaps'] = False
    settings['mapDir'] = 'maps'
    json.dump(settings, f)
    f.close()
else:
    f = open('settings', 'r')
    settings = json.load(f)
    f.close()

if not os.path.isdir(settings['mapDir']):
    os.makedirs(settings['mapDir'])

if settings['preLoadMaps']:
    load_maps()

app = Flask(__name__)
api = Api(app)

api.add_resource(AddMap, '/api/addmap')
api.add_resource(ShortestRoute, '/api/shortest')
api.add_resource(Settings, '/api/settings')
api.add_resource(GetAllMaps, '/api/allmaps')
api.add_resource(GetMap, '/api/map/<string:map_name>')

if __name__ == '__main__':
    app.run(debug=True)